package com.fared.selpraytime;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

/**
 * Created by Admin on 11/4/2016.
 */

public class LocationActivity extends AppCompatActivity {

    ListView lv;
    String regions[] = {"Daerah 1", "Daerah 2"};

    String location[] = {"Gombak, Hulu Selangor, Rawang, Hulu Langat, Sepang, Petaling, Shah Alam",
            "Sabak Bernam, Kuala Selangor, Klang, Kuala Langa"};
    Typeface tf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_location);
        lv = (ListView) findViewById(R.id.list1);


        String fontPath = "fonts/ProximaNova-Semibold.otf";

        tf = Typeface.createFromAsset(getAssets(), fontPath);

        StableArrayAdapter adapter = new StableArrayAdapter(this, R.layout.location_list_view, location, regions);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                Intent i = new Intent(getApplication(), MainActivity.class);
                i.putExtra("kawasan", String.valueOf(position));
                startActivity(i);

//                Toast toast = Toast.makeText(getApplication(), "HAI " + location[position], Toast.LENGTH_LONG);
//                toast.show();
            }

        });

    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        //HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
//        Typeface tf;
        Activity ctx;
        String[] objects;
        String[] region;


        public StableArrayAdapter(Activity context, int textViewResourceId,
                                  String[] objects, String[] objects2) {
            super(context, textViewResourceId, objects);

//            tf = Typeface.createFromAsset(context.getAssets());
            ctx = context;
            this.objects = objects;
            this.region = objects2;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;

            if (rowView == null) {
                LayoutInflater inflater = ctx.getLayoutInflater();
                rowView = inflater.inflate(R.layout.location_list_view, null);
            }

            TextView firstLine = (TextView) rowView.findViewById(R.id.daerahTxt);
            TextView secondLine = (TextView) rowView.findViewById(R.id.desc);

            firstLine.setText(region[position]);
            firstLine.setTypeface(tf);

            secondLine.setText("kawasan terlibat : " + objects [position]);
            secondLine.setTypeface(tf);


            return rowView;

        }
    }
}
