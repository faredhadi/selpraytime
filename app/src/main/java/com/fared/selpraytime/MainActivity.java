package com.fared.selpraytime;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    TextView mTxtDisplay, txtImsak, txtsubuh, txtSyuruk, txtZohor, txtAsar,
            txtMaghrib, txtIsyak, txtDate, txtNow, txtNext;
//    String url = "http://api.kayrules.com/solatjakim/times/today.json?zone=sgr01&format=12-hour";
    String imsak, subuh, syuruk, zohor, asar, maghrib, isyak;
    String data = "";
    String url = "";
    String zone = "";
    int kawasan;

    JSONArray arr = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String fontPath = "fonts/ProximaNova-Semibold.otf";

        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

        mTxtDisplay = (TextView) findViewById(R.id.textlokasi);
        mTxtDisplay.setTypeface(tf);
        txtDate = (TextView) findViewById(R.id.textdate);
        txtDate.setTypeface(tf);
        txtNow = (TextView) findViewById(R.id.waktuskang);
        txtNow.setTypeface(tf);
        txtImsak = (TextView) findViewById(R.id.txt_imsak);
        txtImsak.setTypeface(tf);
        txtsubuh = (TextView) findViewById(R.id.txt_subuh);
        txtsubuh.setTypeface(tf);
        txtSyuruk = (TextView) findViewById(R.id.txt_syuruk);
        txtSyuruk.setTypeface(tf);
        txtZohor = (TextView) findViewById(R.id.txt_zohor);
        txtZohor.setTypeface(tf);
        txtAsar = (TextView) findViewById(R.id.txt_asar);
        txtAsar.setTypeface(tf);
        txtMaghrib = (TextView) findViewById(R.id.txt_maghrib);
        txtMaghrib.setTypeface(tf);
        txtIsyak = (TextView) findViewById(R.id.txt_isyak);
        txtIsyak.setTypeface(tf);

//        String url = "http://api.kayrules.com/solatjakim/times/today.json?zone=sgr01&format=12-hour";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            kawasan = Integer.parseInt(extras.getString("kawasan"));
        }

        switch (kawasan){
            case 0:
                zone = "sgr01";
                break;
            case 1:
                zone = "sgr02";
                break;
        }

        String currentDateTimeString = DateFormat.getDateInstance().format(new Date());
        txtDate.setText(currentDateTimeString);




        Log.d("fared", "Kawasan : " + kawasan);

        if (zone != ""){
            url = "http://api.kayrules.com/solatjakim/times/today.json?zone=" + zone +"&format=24-hour";
            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("fared", response.toString());
                            try {
                                JSONArray arr = response.getJSONArray("locations");
                                JSONObject obj = response.getJSONObject("prayer_times");

//                                String lokasi = arr.;

                                imsak = obj.getString("imsak");
                                subuh = obj.getString("subuh");
                                syuruk = obj.getString("syuruk");
                                zohor = obj.getString("zohor");
                                asar = obj.getString("asar");
                                maghrib = obj.getString("maghrib");
                                isyak = obj.getString("isyak");
//                            String desc = obj.getString("description");

                                Log.d("fared", "arr length = " +arr.length());

                                // Adds strings from object to the "data" string
                                for (int i = 0; i < arr.length(); i++) {
                                    data += arr.get(i).toString() + ", ";
                                    Log.d("fared", data);
                                }




                                // Adds the data string to the TextView "results"
                                mTxtDisplay.setText("Lokasi bagi : " + data);
                                txtImsak.setText(imsak);
                                txtsubuh.setText(subuh);
                                txtSyuruk.setText(syuruk);
                                txtZohor.setText(zohor);
                                txtAsar.setText(asar);
                                txtMaghrib.setText(maghrib);
                                txtIsyak.setText(isyak);

                                txtNow.setText("Sekarang Waktu : " + waktuSekarang());
                            }
                            // Try and catch are included to handle any errors due to JSON
                            catch (JSONException e) {
                                // If an error occurs, this prints the error to the log
                                e.printStackTrace();
                            }
//                        mTxtDisplay.setText("Response: " + response.toString());
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub

                        }
                    });

            // Access the RequestQueue through your singleton class.
            Singleton.getInstance(this).addToRequestQueue(jsObjRequest);


        }

//        waktuSekarang();


    }

    public String waktuSekarang(){

        String waktuSolat = "";

//        String time = DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date());

        SimpleDateFormat sdf = new SimpleDateFormat("kk:mm");
        String time = sdf.format(new Date());

//        time.replaceAll("[^\\d.]", "");

        Log.d("fared", "TIME = " +time.replaceAll("[^\\d.]", ""));

        int currTime = Integer.valueOf(time.replaceAll("[^\\d.]", ""));

        Log.d("fared", "Subuh = " +subuh);

        if(currTime >= Integer.valueOf(subuh.replaceAll("[^\\d.]", "")) && currTime < Integer.valueOf(syuruk.replaceAll("[^\\d.]", ""))) {
            waktuSolat = "Subuh";
        } else if (currTime >= Integer.valueOf(syuruk.replaceAll("[^\\d.]", "")) && currTime < Integer.valueOf(zohor.replaceAll("[^\\d.]", ""))){
            waktuSolat = "Syuruk";
        } else if (currTime >= Integer.valueOf(zohor.replaceAll("[^\\d.]", "")) && currTime < Integer.valueOf(asar.replaceAll("[^\\d.]", ""))){
            waktuSolat = "Zohor";
        } else if (currTime >= Integer.valueOf(asar.replaceAll("[^\\d.]", "")) && currTime < Integer.valueOf(maghrib.replaceAll("[^\\d.]", ""))){
            waktuSolat = "Asar";
        } else if (currTime >= Integer.valueOf(maghrib.replaceAll("[^\\d.]", "")) && currTime < Integer.valueOf(isyak.replaceAll("[^\\d.]", ""))) {
            waktuSolat = "Maghrib";
//        } else (currTime >= Integer.valueOf(isyak.replaceAll("[^\\d.]", "")) && currTime < Integer.valueOf(imsak.replaceAll("[^\\d.]", ""))){
//            waktuSolat = "Isyak";
//        }
        } else {
            waktuSolat = "Isyak";
        }

        Log.d("fared", "waktu sekarang " +waktuSolat);

        return waktuSolat;
    }
}
